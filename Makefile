.PHONY: build
build:
	cd ext && \
        go build -buildmode=c-shared -o ../lib/devfile.so main.go

.PHONY: gem-build
gem-build:
	gem build devfile_ffi.gemspec

.PHONY: gem-install
gem-install: gem-uninstall gem-build
	gem install ./devfile_ffi-0.0.2.gem

.PHONY: gem-uninstall
gem-uninstall:
	gem uninstall devfile_ffi --version 0.0.2

.PHONY: test
test: build
	./bin/rspec