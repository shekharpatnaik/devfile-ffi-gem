# Ruby Devfile Gem

This gem generates Kubernetes yamls from a [Devfile](https://devfile.io). This gem wraps the existing go [Devfile library](https://github.com/devfile/library) using FFI.

# Example

```ruby
require 'devfile'

devfile = %{
schemaVersion: 2.1.0
metadata:
  name: go
  language: go
components:
  - container:
      endpoints:
        - name: http
          targetPort: 8080
      image: quay.io/devfile/golang:latest
      memoryLimit: 1024Mi
      mountSources: true
    name: runtime
}

df = Devfile::Parser.new(devfile)
deployment = df.get_deployment("test", "default")
puts deployment
```