require 'ffi'

module Devfile
	extend FFI::Library

	ffi_lib File.expand_path("./devfile.so", File.dirname(__FILE__))

	class Result < FFI::Struct
		layout :value, :string, :err, :string
	end

	class Parser < FFI::Struct

		layout :contents, :pointer

		def initialize(contents)
			self[:contents] = FFI::MemoryPointer.from_string(contents)
		end

		def get_deployment(name, namespace)
			result = Devfile.getDeployment(self, name, namespace)
			raise result[:err] if result[:err]
			result[:value]
		end
	end

	attach_function 'getDeployment', [Parser.by_value, :string, :string], Result.by_value
end