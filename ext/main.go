package main

/*
struct parser {
	char *contents;
};

struct result {
	char *value;
	char *err;
};
*/
import "C"
import (
	"bytes"

	"github.com/devfile/library/pkg/devfile/generator"
	"github.com/devfile/library/pkg/devfile/parser"
	"github.com/devfile/library/pkg/devfile/parser/data/v2/common"
	"k8s.io/cli-runtime/pkg/printers"
)

func main() {}

type Devfile C.struct_parser
type Result C.struct_result

//export getDeployment
func (f Devfile) getDeployment(name *C.char, namespace *C.char) Result {

	parserArgs := parser.ParserArgs{
		Data: []byte(C.GoString(f.contents)),
	}

	devfile, err := parser.ParseDevfile(parserArgs)
	if err != nil {
		return Result{
			value: nil,
			err:   C.CString(err.Error()),
		}
	}

	containers, err := generator.GetContainers(devfile, common.DevfileOptions{})
	if err != nil {
		return Result{
			value: nil,
			err:   C.CString(err.Error()),
		}
	}

	deployParams := generator.DeploymentParams{
		TypeMeta:   generator.GetTypeMeta("Deployment", "apps/v1"),
		ObjectMeta: generator.GetObjectMeta(C.GoString(name), C.GoString(namespace), map[string]string{}, make(map[string]string)),
		Containers: containers,
		PodSelectorLabels: map[string]string{
			"name": C.GoString(name),
		},
	}

	deployment, err := generator.GetDeployment(devfile, deployParams)
	if err != nil {
		return Result{
			value: nil,
			err:   C.CString(err.Error()),
		}
	}

	printer := printers.YAMLPrinter{}
	dest := bytes.NewBuffer([]byte{})
	err = printer.PrintObj(deployment, dest)
	if err != nil {
		return Result{
			value: nil,
			err:   C.CString(err.Error()),
		}
	}

	return Result{
		value: C.CString(dest.String()),
		err:   nil,
	}
}
