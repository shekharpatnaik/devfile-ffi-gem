require 'devfile'
require 'yaml'

RSpec.describe Devfile, '#get_deployment' do
    context "when a valid devfile is provided" do
        it "#get_deployment returns a valid kubernetes deployment yaml" do
            sample_file = %{
            schemaVersion: 2.1.0
            metadata:
                name: go
                language: go
            components:
            - container:
                endpoints:
                - name: http
                  targetPort: 8080
                image: quay.io/devfile/golang:latest
                memoryLimit: 1024Mi
                mountSources: true
              name: runtime    
            }

            expected_result = YAML::load(%{
            apiVersion: apps/v1
            kind: Deployment
            metadata:
              creationTimestamp: null
              name: test
              namespace: default
            spec:
                selector:
                    matchLabels:
                        name: test
                strategy:
                    type: Recreate
                template:
                    metadata:
                        creationTimestamp: null
                        name: test
                        namespace: default
                    spec:
                        containers:
                        - env:
                            - name: PROJECTS_ROOT
                              value: /projects
                            - name: PROJECT_SOURCE
                              value: /projects
                          image: quay.io/devfile/golang:latest
                          imagePullPolicy: Always
                          name: runtime
                          ports:
                          - containerPort: 8080
                            name: http
                            protocol: TCP
                          resources:
                            limits:
                              memory: 1Gi
            status: {}
            })

            df = Devfile::Parser.new(sample_file)
            actual_result = YAML::load(df.get_deployment("test", "default"))

            expect(actual_result).to  eq expected_result
        end
    end
    
    context "when an invalid devfile is provided" do
        it "#get_deployment throws an error" do
            sample_file = %{
            random: field
            schemaVersion: 2.1.0
            metadata:
                name: go
                language: go
            components:
            - container:
                endpoints:
                - name: http
                  targetPort: 8080
                image: quay.io/devfile/golang:latest
                memoryLimit: 1024Mi
                mountSources: true
              name: runtime    
            }

            df = Devfile::Parser.new(sample_file)
            expect { df.get_deployment("test", "default") }.to raise_error(RuntimeError, /Additional property random is not allowed/)
        end
    end
end