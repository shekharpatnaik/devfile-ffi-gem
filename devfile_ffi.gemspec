Gem::Specification.new do |s|
  s.name        = 'devfile_ffi'
  s.version     = '0.0.2'
  s.licenses    = ['MIT']
  s.summary     = "Parse and generate kubernetes manifests from a Devfile"
  s.description = "Library used to generate kubernetes manifests from a Devfile. Based on "
  s.authors     = ["GitLab"]
  s.email       = 'spatnaik@gitlab.com'
  s.files       = ["lib/devfile.rb", "ext/extconf.rb", "ext/go.mod", "ext/go.sum", "ext/main.go"]
  s.homepage    = 'https://gitlab.com'
  s.metadata    = { "source_code_uri" => "https://github.com/example/example" }
  s.extensions << './ext/extconf.rb'
end